This project was created for the needs of [TEDxUniversityofMacedonia 2018](https://www.tedxuniversityofmacedonia.com/el/)

# Installation Instructions

You need the latest Nodejs LTS version 
It's also recommended to install Yarn for faster package management

After you install the prerequisites run
```
yarn
```

to install the required modules.

Afterwards you can run  the application running with 
```
yarn start
```

To build for production run
```
yarn build
```
You will find the production-ready files in `build/`

# Database

This application works with a Firebase DB.

Create a new Firebase project and after opening select Database from the sidebar.

Go to the rules tab and change them to 
```json
{
  "rules": {
    "application": {
        "$uid" : {
           ".read": false,
    	   ".write": false,
        }
    }  
  }
}
```

You'll need your brand new database's API keys
Select the Settings icon from the sidebar, choose Project Settings.
You'll need the following fields
- Web API Key 
- Public-facing name (the numbers only)
- Project ID 

Enter your new credentials at /src/constants/firebase.js
Change every `tedxuom-volunteer-fom` with your new project-id



