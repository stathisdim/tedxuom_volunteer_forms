import React from "react";
import PropTypes from "prop-types";
import Form from "./Form";
import "./TranslationTeam.css";

const TranslationTeam = ({
  handleDataUpdate,
  handleSubmit,
  answers,
  history
}) => {
  return (
    <div className="translation-team">
      <div className="translation-header">
        <h2>Translation Team</h2>
        <p>
          Αγαπάς κι εσύ τη μετάφραση ; Θέλεις να ακουστούν οι ιδέες μας εκτός
          συνόρων; Σε αυτή την ομάδα θα είσαι υπεύθυνος για την
          απομαγνητοσκόπηση ομιλιών του TEDxUniversityofMacedonia στη
          μεταφραστική κοινότητα του TED και σταδιακά στη μετάφρασή τους.
          Ετοιμάσου κι εσύ να βοηθήσεις ώστε να φτάσουν οι σκέψεις μας σε κάθε
          γωνιά του πλανήτη και να γίνεις κομμάτι του 6ου
          TEDxUniversityofMacedonia.
        </p>
      </div>
      <Form submit={() => handleSubmit(history)}>
        <label>
          1.Γιατί ενδιαφέρεσαι γι’ αυτή τη θέση;
          <span className="textarea-margin" />
          <textarea name="preference_why" onKeyUp={handleDataUpdate} required />
        </label>
        <label>
          2.Ποιες δικές σου γνώσεις (π.χ. πολύ καλή γνώση Αγγλικών) και
          χαρακτηριστικά, νομίζεις ότι μπορούν να συμβάλλουν στο εγχείρημα της
          συγκεκριμένης ομάδας;
          <span className="textarea-margin" />
          <textarea
            name="knowledge_how_can_help"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label>
          3.Έχετε κάνει κάτι παρόμοιο στο παρελθόν; Αν ναι, τι ήταν αυτό;
          <span className="textarea-margin" />
          <textarea
            name="previous_experience"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label className="choice">
          Θα σε ενδιέφερε κάποια άλλη θέση στην ομάδα εθελοντών του
          TEDxUniversityofMacedonia;
          <div>
            <input
              type="radio"
              id="teamChoice1"
              name="secondary_team_choice"
              value="Blog Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice1">Blog Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice2"
              name="secondary_team_choice"
              value="Runners Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice2">Runners Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice3"
              name="secondary_team_choice"
              value="Photo Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice3">Photo Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice4"
              name="secondary_team_choice"
              value="Art Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice4">Art Team</label>
          </div>
        </label>
        <button type="submit">Υποβολή</button>
      </Form>
    </div>
  );
};

TranslationTeam.propTypes = {
  handleDataUpdate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  answers: PropTypes.object.isRequired
};

export default TranslationTeam;
