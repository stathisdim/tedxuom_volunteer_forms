import React from "react";
import PropTypes from "prop-types";
import Form from "./Form";
import "./TranslationTeam.css";

const RunnersTeam = ({ handleDataUpdate, handleSubmit, answers, history }) => {
  return (
    <div className="translation-team">
      <div className="translation-header">
        <h2>Runners Team</h2>
        <p>
          Συμμετοχή σε pre-events και δράσεις, workshops, σκηνικά, παραγωγή,
          μεταφορά ομιλητών. Είμαστε η κοινότητα που συμμετέχοντας ενεργά μέχρι
          το τέλος, θα υλοποιήσουμε το 6ο TEDxUniversityofMacedonia με βασικά
          χαρακτηριστικά την διάθεση, την συνεργασία και το ενδιαφέρον για την
          ομάδα. Είσαι έτοιμος να γίνεις κομμάτι της εμπειρίας του
          TEDxUniversityofMacedonia;
        </p>
      </div>
      <Form submit={() => handleSubmit(history)}>
        <label>
          1.Τι σημαίνει για σένα η έννοια του εθελοντισμού;
          <span className="textarea-margin" />
          <textarea
            name="meaning_of_volunteering"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label>
          2.Τι άλλες γνώσεις διαθέτεις τις οποίες θα μπορούσες να
          χρησιμοποιήσεις στην διοργάνωση του TEDxUniversityofMacedonia;
          <span className="textarea-margin" />
          <textarea
            name="knowledge_how_can_help"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label className="choice">
          Θα σε ενδιέφερε κάποια άλλη θέση στην ομάδα εθελοντών του
          TEDxUniversityofMacedonia;
          <div>
            <input
              type="radio"
              id="teamChoice1"
              name="secondary_team_choice"
              value="Blog Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice1">Blog Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice2"
              name="secondary_team_choice"
              value="Translation Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice2">Translation Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice3"
              name="secondary_team_choice"
              value="Photo Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice3">Photo Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice4"
              name="secondary_team_choice"
              value="Art Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice4">Art Team</label>
          </div>
        </label>
        <button type="submit">Υποβολή</button>
      </Form>
    </div>
  );
};

RunnersTeam.propTypes = {
  handleDataUpdate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  answers: PropTypes.object.isRequired
};

export default RunnersTeam;
