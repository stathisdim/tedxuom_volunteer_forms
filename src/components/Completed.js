import React from "react";
import {Link} from "react-router-dom";
import "./Completed.css";

export default () => {
  return (
    <div className="completed">
      Σε ευχαριστούμε για το ενδιαφέρον σου. Θα έχεις νέα μας σύντομα, σου
      υπενθυμίζουμε ότι οι συνεντεύξεις θα πραγματοποιηθούν <strong>από τις 25 Απριλίου
        έως και τις 5 Μαΐου.</strong>
      <button onClick={e => e.preventDefault()} style={{maxWidth: '160px', margin: '2vh auto'}}>
          <Link to="/">Επιστροφή</Link>
        </button>
    </div>
  );
};
