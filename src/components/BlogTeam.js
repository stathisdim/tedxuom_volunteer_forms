import React, { Component } from "react";
import PropTypes from "prop-types";
import Form from "./Form";
import "./TranslationTeam.css";

class BlogTeam extends Component {
  state = {
    count: 0
  };

  handleInterestsChange = e => {
    let { count } = this.state;
    if (e.target.checked) {
      if (count >= 3) {
        e.target.checked = false;
      } else {
        count++;
      }
    } else {
      count--;
    }
    this.setState({ count });

    this.props.handleDataUpdate(e);
  };

  render() {
    const { handleDataUpdate, handleSubmit, answers, history } = this.props;
    return (
      <div className="translation-team">
        <div className="translation-header">
          <h2>Blog Team</h2>
          <p>
            Σου αρέσει να γράφεις με τον δικό σου μοναδικό τρόπο; Επιθυμείς να
            ενημερώνεις και να εκφράζεις τις σκέψεις σου; Είτε το κάνεις για να
            κερδίζεις ή να διασκεδάζεις τους αναγνώστες σου, είτε επειδή σου
            αρέσει, να η ευκαιρία για το πρώτο σου μεγάλο βήμα . Η οικογένεια
            του TEDxUniversityofMacedonia αναζητά νέα μέλη που θα κληθούν να
            είναι υπεύθυνα για τη σύνταξη άρθρων που θα διασκεδάσουν, θα
            αγαπηθούν και θα μας μείνουν. Ανεξάρτητα με το αν έχεις πρακτική
            εμπειρία ή όχι ετοιμάσου να μοιραστείς τις ιδέες σου για τη
            μεγαλύτερη φοιτητική πλατφόρμα ανταλλαγής ιδεών στη πόλη σου!
          </p>
        </div>
        <Form submit={() => handleSubmit(history)}>
          <label className="choice">
            1. Επίλεξε 3 κατηγορίες που εκφράζουν περισσότερο τα ενδιαφέροντα
            σου.
            <div>
              <input
                type="checkbox"
                id="interestsChoice1"
                name="interests"
                value="Άνθρωποι/ Κοινωνία"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice1">Άνθρωποι/ Κοινωνία</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="interestsChoice2"
                name="interests"
                value="Πληροφορική"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice2">Πληροφορική</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="interestsChoice3"
                name="interests"
                value="Οικονομικά/ Επιχειρήσεις"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice3">Οικονομικά/ Επιχειρήσεις</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="interestsChoice4"
                name="interests"
                value="Ταξίδια"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice4">Ταξίδια</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="interestsChoice5"
                name="interests"
                value="Αθλητισμός"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice5">Αθλητισμός</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="interestsChoice6"
                name="interests"
                value="Ψυχαγωγία/Διασκέδαση"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice6">Ψυχαγωγία/Διασκέδαση</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="interestsChoice7"
                name="interests"
                value="Επιστήμη"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice7">Επιστήμη</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="interestsChoice8"
                name="interests"
                value="Περιβάλλον"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice8">Περιβάλλον</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="interestsChoice9"
                name="interests"
                value="Τέχνες (Αρχιτεκτονική, Κινηματογράφος, Μουσική, Θέατρο κ.ά.)"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice9">
                Τέχνες (Αρχιτεκτονική, Κινηματογράφος, Μουσική, Θέατρο κ.ά.)
              </label>
            </div>
            <div>
              <input
                type="checkbox"
                id="interestsChoice10"
                name="interests"
                value="Επικοινωνία/ ΜΜΕ"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice10">Επικοινωνία/ ΜΜΕ</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="interestsChoice11"
                name="interests"
                value="Πολιτικά"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice11">Πολιτικά</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="interestsChoice12"
                name="interests"
                value="Άλλο"
                onClick={this.handleInterestsChange}
              />
              <label htmlFor="interestsChoice12" id="radio_with_text">
                Άλλο
                <input type="text" name="interests_other" onKeyUp={handleDataUpdate}/>
              </label>
            </div>
          </label>
          <label>
            2.Διάλεξε ένα βίντεο. Να φροντίσεις απλά να μην είναι μεγαλύτερο των
            πέντε λεπτών και να αντιστοιχεί με τα ενδιαφέροντά σου. Στείλε μας
            τον σύνδεσμο του βίντεο και σχολίασε το με τον δικό σου τρόπο
            έκφρασης!
            <span className="textarea-margin" />
            <textarea name="chosen_video" onKeyUp={handleDataUpdate} required />
          </label>
          <label className="choice">
            3. Έχεις γνώσεις:
            <div>
              <input
                type="checkbox"
                id="programChoice1"
                name="application_experienced"
                value="Photoshop"
                onChange={handleDataUpdate}
              />
              <label htmlFor="programChoice1">Photoshop</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="programChoice2"
                name="application_experienced"
                value="Videomaking"
                onChange={handleDataUpdate}
              />
              <label htmlFor="programChoice2">Videomaking</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="programChoice3"
                name="application_experienced"
                value="Αγγλικά σε υψηλό επίπεδο"
                onChange={handleDataUpdate}
              />
              <label htmlFor="programChoice3">Αγγλικά σε υψηλό επίπεδο</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="programChoice4"
                name="application_experienced"
                value="Wordpress"
                onChange={handleDataUpdate}
              />
              <label htmlFor="programChoice4">Wordpress</label>
            </div>
            <div>
              <input
                type="checkbox"
                id="programChoice5"
                name="application_experienced"
                value="Άλλο"
                onChange={handleDataUpdate}
              />
              <label htmlFor="programChoice5" id="radio_with_text">
                Άλλο
                <input type="text" name="application_experienced_other" onKeyUp={handleDataUpdate} />
              </label>
            </div>
          </label>
          <label>
            4. Εάν θέλεις, κάνε επικόλληση παρακάτω έναν σύνδεσμο που θα
            περιέχει ένα άρθρο που έχεις γράψει και το αγάπησες λίγο περισσότερο
            από τα άλλα. Το περιεχόμενο του είναι καθαρά δική σου επιλογή!
            <span className="textarea-margin" />
            <textarea name="chosen_article" onKeyUp={handleDataUpdate} />
          </label>
          <label className="choice">
            5. Θα σε ενδιέφερε κάποια άλλη θέση στην ομάδα εθελοντών του
            TEDxUniversityofMacedonia;
            <div>
              <input
                type="radio"
                id="teamChoice1"
                name="secondary_team_choice"
                value="Art Team"
                onChange={handleDataUpdate}
                required
              />
              <label htmlFor="teamChoice1">Art Team</label>
            </div>
            <div>
              <input
                type="radio"
                id="teamChoice2"
                name="secondary_team_choice"
                value="Translation Team"
                onChange={handleDataUpdate}
                required
              />
              <label htmlFor="teamChoice2">Translation Team</label>
            </div>
            <div>
              <input
                type="radio"
                id="teamChoice3"
                name="secondary_team_choice"
                value="Photo Team"
                onChange={handleDataUpdate}
                required
              />
              <label htmlFor="teamChoice3">Photo Team</label>
            </div>
            <div>
              <input
                type="radio"
                id="teamChoice4"
                name="secondary_team_choice"
                value="Runners Team"
                onChange={handleDataUpdate}
                required
              />
              <label htmlFor="teamChoice4">Runners Team</label>
            </div>
          </label>
          <button type="submit">Υποβολή</button>
        </Form>
      </div>
    );
  }
}

BlogTeam.propTypes = {
  handleDataUpdate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  answers: PropTypes.object.isRequired
};

export default BlogTeam;
