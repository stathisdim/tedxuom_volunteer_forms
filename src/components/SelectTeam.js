import React from "react";
import PropTypes from "prop-types";
import Form from "./Form";
import "./SelectTeam.css";

const createDestination = choiceName => {
  const formattedChoice = choiceName.split(" ")[0].toLowerCase();
  console.log(formattedChoice);
  return formattedChoice;
};

const SelectTeam = ({ handleDataUpdate, handleSubmit, answers, history }) => {
  return (
    <div className="select-team">
      <h1>Σε ποια ομάδα θα σου άρεσε περισσότερο να συμμετέχεις;</h1>
      <div>
        <h2>Translation Team</h2>
        <p>
          Αγαπάς κι εσύ τη μετάφραση ; Θέλεις να ακουστούν οι ιδέες μας εκτός
          συνόρων; Σε αυτή την ομάδα θα είσαι υπεύθυνος για την
          απομαγνητοσκόπηση ομιλιών του TEDxUniversityofMacedonia στη
          μεταφραστική κοινότητα του TED και σταδιακά στη μετάφρασή τους.
          Ετοιμάσου κι εσύ να βοηθήσεις ώστε να φτάσουν οι σκέψεις μας σε κάθε
          γωνιά του πλανήτη και να γίνεις κομμάτι του 6ου
          TEDxUniversityofMacedonia.
        </p>
      </div>
      <div>
        <h2>Blog Team</h2>
        <p>
          Σου αρέσει να γράφεις με τον δικό σου μοναδικό τρόπο; Επιθυμείς να
          ενημερώνεις και να εκφράζεις τις σκέψεις σου; Είτε το κάνεις για να
          κερδίζεις ή να διασκεδάζεις τους αναγνώστες σου, είτε επειδή σου
          αρέσει, να η ευκαιρία για το πρώτο σου μεγάλο βήμα . Η οικογένεια του
          TEDxUniversityofMacedonia αναζητά νέα μέλη που θα κληθούν να είναι
          υπεύθυνα για τη σύνταξη άρθρων που θα διασκεδάσουν, θα αγαπηθούν και
          θα μας μείνουν. Ανεξάρτητα με το αν έχεις πρακτική εμπειρία ή όχι
          ετοιμάσου να μοιραστείς τις ιδέες σου για τη μεγαλύτερη φοιτητική
          πλατφόρμα ανταλλαγής ιδεών στη πόλη σου!
        </p>
      </div>
      <div>
        <h2>Runners Team</h2>
        <p>
          Συμμετοχή σε pre-events και δράσεις, workshops, σκηνικά, παραγωγή,
          μεταφορά ομιλητών. Είμαστε η κοινότητα που συμμετέχοντας ενεργά μέχρι
          το τέλος, θα υλοποιήσουμε το 6ο TEDxUniversityofMacedonia με βασικά
          χαρακτηριστικά την διάθεση, την συνεργασία και το ενδιαφέρον για την
          ομάδα. Είσαι έτοιμος να γίνεις κομμάτι της εμπειρίας του
          TEDxUniversityofMacedonia;
        </p>
      </div>
      <div>
        <h2>Photo Team</h2>
        <p>
          Με τη συμμετοχή σου στην ομάδα αυτή θα έχεις τη δυνατότητα να
          απαθανατίσεις κάθε στιγμή των pre και after events, ενώ θα κληθείς να
          καλύψεις φωτογραφικά και το main event! Θα ασχοληθείς, κατά κύριο
          λόγο, με τη φωτογραφία καθώς και το video making, όμως θα είσαι και
          ενεργό κομμάτι της ευρύτερης ομάδας των εθελοντών. Πάρε τη φωτογραφική
          σου μηχανή, βάλε τη δημιουργικότητά σου στο τέρμα και ετοιμάσου να
          δείξεις στον κόσμο τι σημαίνει να είσαι μέρος του 6ου
          TEDxUniversityofMacedonia!
        </p>
      </div>
      <div>
        <h2>Art Team</h2>
        <p>
          Εάν είσαι δημιουργική ψυχή και παράλληλα το άτομο που κάθε βδομάδα
          αλλάζει θέση τα έπιπλα στο σπίτι, τότε ανήκεις σε αυτήν την ομάδα! Η
          Art team, δημιουργήθηκε για την χωροθετική -καλλιτεχνική οργάνωση των
          pre-events, και φυσικά την επιμέλεια και τον σχεδιασμό των σκηνικών
          και του χώρου του μεγάλου μας event. Σε αυτή την ομάδα, θα έχεις τη
          δυνατότητα να συμβάλλεις στη δημιουργική διαδικασία της οργάνωσης του
          TEDxUniversityofMacedonia, επιστρατεύοντας τις γνώσεις, την
          ευρηματικότητα και το ταλέντο σου!
        </p>
      </div>
      <div className="select-team-form">
        <Form
          submit={() =>
            handleSubmit(history, `/${createDestination(answers.team_choice)}`)
          }
        >
          <label className="choice">
            Σε ποια ομάδα θα σου άρεσε περισσότερο να συμμετέχεις;
            <div>
              <input
                type="radio"
                id="teamChoice1"
                name="team_choice"
                value="Translation Team"
                onChange={handleDataUpdate}
                required
              />
              <label htmlFor="teamChoice1">Translation Team</label>
            </div>
            <div>
              <input
                type="radio"
                id="teamChoice2"
                name="team_choice"
                value="Blog Team"
                onChange={handleDataUpdate}
                required
              />
              <label htmlFor="teamChoice2">Blog Team</label>
            </div>
            <div>
              <input
                type="radio"
                id="teamChoice3"
                name="team_choice"
                value="Runners Team"
                onChange={handleDataUpdate}
                required
              />
              <label htmlFor="teamChoice3">Runners Team</label>
            </div>
            <div>
              <input
                type="radio"
                id="teamChoice4"
                name="team_choice"
                value="Photo Team"
                onChange={handleDataUpdate}
                required
              />
              <label htmlFor="teamChoice4">Photo Team</label>
            </div>
            <div>
              <input
                type="radio"
                id="teamChoice5"
                name="team_choice"
                value="Art Team"
                onChange={handleDataUpdate}
                required
              />
              <label htmlFor="teamChoice5">Art Team</label>
            </div>
          </label>
          <button type="submit">Επόμενο</button>
        </Form>
      </div>
    </div>
  );
};

SelectTeam.propTypes = {
  handleDataUpdate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  answers: PropTypes.object.isRequired
};

export default SelectTeam;
