import React from "react";
import PropTypes from "prop-types";
import Form from "./Form";
import "./TranslationTeam.css";

const PhotoTeam = ({ handleDataUpdate, handleSubmit, answers, history }) => {
  return (
    <div className="translation-team">
      <div className="translation-header">
        <h2>Photo Team</h2>
        <p>
          Με τη συμμετοχή σου στην ομάδα αυτή θα έχεις τη δυνατότητα να
          απαθανατίσεις κάθε στιγμή των pre και after events, ενώ θα κληθείς να
          καλύψεις φωτογραφικά και το main event! Θα ασχοληθείς, κατά κύριο
          λόγο, με τη φωτογραφία καθώς και το video making, όμως θα είσαι και
          ενεργό κομμάτι της ευρύτερης ομάδας των εθελοντών. Πάρε τη φωτογραφική
          σου μηχανή, βάλε τη δημιουργικότητά σου στο τέρμα και ετοιμάσου να
          δείξεις στον κόσμο τι σημαίνει να είσαι μέρος του 6ου
          TEDxUniversityofMacedonia!
        </p>
      </div>
      <Form submit={() => handleSubmit(history)}>
        <label>
          1.Γιατί ενδιαφέρεσαι γι' αυτή τη θέση;
          <span className="textarea-margin" />
          <textarea
            name="why_choose_position"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label>
          2.Έχεις συμμετάσχει σε κάποιο παρόμοιο event ή σε κάποια εθελοντική δράση,στο παρελθόν;
          <span className="textarea-margin" />
          <textarea
            name="previous_experience"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label>
          3. Παρακάτω μπορείς να παραθέσεις έναν σύνδεσμο που θα περιέχει φωτογραφίες που έχεις τραβήξει και τις αγάπησες λίγο περισσότερο από τις υπόλοιπες. Το περιεχόμενο τους είναι καθαρά δική σου επιλογή! (Επιβεβαιώσου πως θα έχουμε πρόσβαση στο σύνδεσμο αυτό. Επίσης μην ανησυχείς για το απόρρητο των φωτογραφιών σου δε θα χρησιμοποιηθούν ούτε θα διαρρεύσουν πουθενά!)
          <span className="textarea-margin" />
          <textarea
            name="photos_demo"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label>
          4. Τι κατά τη γνώμη σου περιλαμβάνει η ιδανική φωτογραφία σε ένα τέτοιο event;
          <span className="textarea-margin" />
          <textarea
            name="ideal_photo"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label>
          5. “Cause we are the tech people” γράψε μας, εάν θέλεις, τι εξοπλισμό διαθέτεις! (φωτογραφική, φακός- οί, εξαρτήματα, κ.α)
          <span className="textarea-margin" />
          <textarea
            name="photo_equipment"
            onKeyUp={handleDataUpdate}
          />
        </label>
        <label className="choice">
          6. Έχεις γνώσεις:
          <div>
            <input
              type="checkbox"
              id="programChoice1"
              name="application_experienced"
              value="Photoshop"
              onChange={handleDataUpdate}
            />
            <label htmlFor="programChoice1">Photoshop</label>
          </div>
          <div>
            <input
              type="checkbox"
              id="programChoice2"
              name="application_experienced"
              value="Lightroom"
              onChange={handleDataUpdate}
            />
            <label htmlFor="programChoice2">Lightroom</label>
          </div>
          <div>
            <input
              type="checkbox"
              id="programChoice3"
              name="application_experienced"
              value="Premiere"
              onChange={handleDataUpdate}
            />
            <label htmlFor="programChoice3">Premiere</label>
          </div>
          <div>
            <input
              type="checkbox"
              id="programChoice4"
              name="application_experienced"
              value="Καμία από τις παραπάνω"
              onChange={handleDataUpdate}
            />
            <label htmlFor="programChoice4">Καμία από τις παραπάνω</label>
          </div>
          <div>
            <input
              type="checkbox"
              id="programChoice5"
              name="application_experienced"
              value="Άλλο"
              onChange={handleDataUpdate}
            />
            <label htmlFor="programChoice5" id="radio_with_text">
              Άλλο
              <input type="text" name="application_experienced_other" onKeyUp={handleDataUpdate} />
            </label>
          </div>
        </label>
        <label className="choice">
          7. Θα σε ενδιέφερε κάποια άλλη θέση στην ομάδα εθελοντών του
          TEDxUniversityofMacedonia;
          <div>
            <input
              type="radio"
              id="teamChoice1"
              name="secondary_team_choice"
              value="Blog Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice1">Blog Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice2"
              name="secondary_team_choice"
              value="Translation Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice2">Translation Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice3"
              name="secondary_team_choice"
              value="Runners Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice3">Runners Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice4"
              name="secondary_team_choice"
              value="Art Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice4">Art Team</label>
          </div>
        </label>
        <button type="submit">Υποβολή</button>
      </Form>
    </div>
  );
};

PhotoTeam.propTypes = {
  handleDataUpdate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  answers: PropTypes.object.isRequired
};

export default PhotoTeam;
