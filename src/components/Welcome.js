import React from "react";
import { Link } from "react-router-dom";
import "./Welcome.css";

export default () => {
  return (
    <div className="welcome-container">
      <p className="welcome-header">
        Ήρθε η ώρα να συμπληρώσεις την αίτηση για να γίνεις{" "}
        <strong>εθελοντής στο TEDxUniversityofMacedonia 2018!</strong>
      </p>
      <p className="welcome-guide">
        Οι αιτήσεις θα μείνουν ανοιχτές μέχρι και τo{" "}
        <strong>Σάββατο 28 Απριλίου και ώρα, 23:59.</strong>
      </p>
      <p className="welcome-guide">
        Τα βήματα είναι απλά:
        <ol>
          <li>Συμπληρώνεις τις ερωτήσεις της αίτησης.</li>
          <li>
            Μετά την επιτυχημένη καταχώρηση της αίτησης σου, θα σου αποσταλεί
            ενημερωτικό e-mail ώστε να επιλέξεις την ημέρα και την ώρα της
            συνέντευξης σου καθώς και να ενημερωθείς για τον χώρο διεξαγωγής. Οι
            συνεντεύξεις θα πραγματοποιηθούν 
            <strong> από τις 25 Απριλίου έως και τις 5 Μαΐου.</strong>
          </li>
        </ol>
      </p>
      <p />
      <div className="welcome-buttons">
        <a className="button" onClick={e => e.preventDefault()}>
          <Link to="/intro">Start</Link>
        </a>
      </div>
    </div>
  );
};
