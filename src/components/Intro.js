import React from "react";
import PropTypes from "prop-types";
import Form from "./Form";
import "./Intro.css";

const Intro = ({ handleDataUpdate, handleSubmit, answers, history }) => {
  return (
    <div className="intro">
      <h1>Προσωπικά στοιχεία</h1>

      <Form submit={() => handleSubmit(history, "/selectTeam")}>
        <label>
          1. Ποιο είναι το όνομα σου;
          <input
            type="text"
            placeholder="Όνομα"
            name="name"
            autoComplete="off"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label>
          2. Ποιο είναι το επώνυμο σου;
          <input
            type="text"
            placeholder="Επώνυμο"
            name="last_name"
            autoComplete="off"
            onChange={handleDataUpdate}
            required
          />
        </label>
        <label>
          3. Πότε γεννήθηκες;
          <input
            id="date"
            type="date"
            name="birth_date"
            autoComplete="off"
            onChange={handleDataUpdate}
            required
          />
        </label>
        <label>
          4. Σε ποια σχολή σπουδάζεις (π.χ. Οικονομικό/ΑΠΘ);
          <input
            type="text"
            placeholder="Σχολή"
            name="school"
            autoComplete="off"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label>
          5. Ποιο είναι το email σου;
          <input
            type="email"
            placeholder="example@example.com"
            name="email"
            autoComplete="off"
            onChange={handleDataUpdate}
            required
          />
        </label>
        <label>
          6. Ποιο είναι το κινητό σου τηλέφωνο;
          <input
            type="tel"
            placeholder="6900000000"
            autoComplete="off"
            name="telephone"
            onChange={handleDataUpdate}
            required
          />
        </label>
        <label className="choice">
          7. Έχεις δίπλωμα οδήγησης;
          <div>
            <input
              type="radio"
              id="licenseChoice1"
              name="driver_license"
              value="yes"
              onChange={handleDataUpdate}              
              required
            />
            <label htmlFor="licenseChoice1">Ναι</label>
          </div>
          <div>
            <input
              type="radio"
              id="licenseChoice2"
              name="driver_license"
              value="no"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="licenseChoice2">Όχι</label>
          </div>
        </label>
        <label>
          8. Ποιο είναι το κίνητρο σου για να συμμετέχεις στο φετινό
          TEDxUniversityofMacedonia;
          <span className="textarea-margin" />
          <textarea name="motivation" onKeyUp={handleDataUpdate} required />
        </label>
        <label className="choice">
          9. ΄Εχεις συμμετάσχει σε άλλες εθελοντικές δραστηριότητες;
          <div>
            <input
              type="radio"
              id="otherActivities1"
              name="other_volunteer_activities"
              value="yes"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="otherActivities1">Ναί</label>
          </div>
          <div>
            <input
              type="radio"
              id="otherActivities2"
              name="other_volunteer_activities"
              value="no"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="otherActivities2">Όχι</label>
          </div>
        </label>
        <label>
          10. Εάν ναι ποιές (γράψε μια μικρή περιγραφή του τι ακριβώς έκανες) ;
          <span className="textarea-margin" />
          <textarea
            name="other_volunteer_activities_details"
            onKeyUp={handleDataUpdate}
            disabled={answers.other_volunteer_activities !== "yes"}
            required
          />
        </label>
        <label>
          11. Τι γνωρίζεις για το TED και το TEDx;
          <span className="textarea-margin" />
          <textarea name="tedKnowledge" onKeyUp={handleDataUpdate} required />
        </label>
        <label className="choice">
          12. Από που ενημερωθήκες για τις αιτήσεις εθελοντών;
          <div>
            <input
              type="radio"
              id="informLocation1"
              name="where_find_applications"
              value="social media"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="informLocation1">
              Social media (Facebook/Instagram/Twitter)
            </label>
          </div>
          <div>
            <input
              type="radio"
              id="informLocation2"
              name="where_find_applications"
              value="Φίλοι/Γνωστοί"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="informLocation2">Φίλοι/Γνωστοί</label>
          </div>
          <div>
            <input
              type="radio"
              id="informLocation3"
              name="where_find_applications"
              value="Πανεπιστημιακή κοινότητα"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="informLocation3">Πανεπιστημιακή κοινότητα</label>
          </div>
          <div>
            <input
              type="radio"
              id="informLocation4"
              name="where_find_applications"
              value="TEDxUniversityofMacedonia Website/Blog"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="informLocation4">
              TEDxUniversityofMacedonia Website/Blog
            </label>
          </div>
          <div>
            <input
              type="radio"
              id="informLocation4"
              name="where_find_applications"
              value="Άλλο"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="informLocation4" id="radio_with_text">
             Άλλο <input type="text" name="where_find_applications_other" 
             onKeyUp={handleDataUpdate}
             disabled={answers.where_find_applications !== "Άλλο"}/>
            </label>
          </div>
        </label>
        <button type="submit">Επόμενο</button>
      </Form>
    </div>
  );
};

Intro.propTypes = {
  handleDataUpdate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  answers: PropTypes.object.isRequired
};

export default Intro;
