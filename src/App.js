import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import Welcome from "./components/Welcome";
import Intro from "./components/Intro";
import Completed from "./components/Completed";
import SelectTeam from "./components/SelectTeam";
import TranslationTeam from "./components/TranslationTeam";
import RunnersTeam from "./components/RunnersTeam";
import ArtTeam from "./components/ArtTeam";
import PhotoTeam from "./components/PhotoTeam";
import BlogTeam from "./components/BlogTeam";
import firebase from "./constants/firebase";
import uuidv4 from "uuid/v4";
import "./App.css";

class App extends Component {
  state = {};

  updateState = ({ target }) => {
    let value = target.value;
    const { name } = target;
    if (target.type === "checkbox") {
      const oldState = this.state[name] || [];
      if (target.checked) {
        oldState.push(value)
      } else {
        oldState.pop(value)
      }
      value = oldState;
    }
    this.setState({ [name]: value });
  };

  updateDatabase = history => {
    this.getTimestamp();
    const id = uuidv4();
    const results = this.state;
    results["timestamp"] = this.getTimestamp();
    console.log(results)
    firebase
      .database()
      .ref(`application/${id}`)
      .set({ ...results });
    history.push("/completed");
  };

  getTimestamp = () => {
    //IE8 compatibility mixin
    if (!Date.now) {
      Date.now = function() { return new Date().getTime(); }
    }
    return Date.now(); //seconds
  }

  handleNext = (history, destination) => {
    history.push(destination);
  }

  RouteWithProps = ({
    path,
    exact,
    strict,
    component: Component,
    location,
    ...rest
  }) => (
    <Route
      path={path}
      exact={exact}
      strict={strict}
      location={location}
      render={props => <Component {...props} {...rest} />}
    />
  );

  render() {
    const { RouteWithProps } = this;
    return (
      <div className="App">
        <Switch>
          <Route exact path="/" component={Welcome} />
          <Route exact path="/completed" component={Completed} />
          <RouteWithProps
            path="/intro"
            component={Intro}
            handleDataUpdate={this.updateState.bind(this)}
            handleSubmit={this.handleNext.bind(this)}
            answers={this.state}
          />
          <RouteWithProps
            exact path="/selectTeam"
            component={SelectTeam}
            handleDataUpdate={this.updateState.bind(this)}
            handleSubmit={this.handleNext.bind(this)}
            answers={this.state}
          />
          <RouteWithProps
            exact path="/translation"
            component={TranslationTeam}
            handleDataUpdate={this.updateState.bind(this)}
            handleSubmit={this.updateDatabase.bind(this)}
            answers={this.state}
          />
          <RouteWithProps
            exact path="/runners"
            component={RunnersTeam}
            handleDataUpdate={this.updateState.bind(this)}
            handleSubmit={this.updateDatabase.bind(this)}
            answers={this.state}
          />
          <RouteWithProps
            exact path="/art"
            component={ArtTeam}
            handleDataUpdate={this.updateState.bind(this)}
            handleSubmit={this.updateDatabase.bind(this)}
            answers={this.state}
          />
          <RouteWithProps
            exact path="/photo"
            component={PhotoTeam}
            handleDataUpdate={this.updateState.bind(this)}
            handleSubmit={this.updateDatabase.bind(this)}
            answers={this.state}
          />
          <RouteWithProps
            exact path="/blog"
            component={BlogTeam}
            handleDataUpdate={this.updateState.bind(this)}
            handleSubmit={this.updateDatabase.bind(this)}
            answers={this.state}
          />
        </Switch>
      </div>
    );
  }
}

export default App;
